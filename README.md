# README #

### Run project ###

```sh
cp userauth/local_settings.base userauth/local_settings.py
```

Install docker, docker-compose
For ubuntu - https://docs.docker.com/install/linux/docker-ce/ubuntu/
Postinstall for ubuntu - https://docs.docker.com/install/linux/linux-postinstall/
Install docker-compose - https://docs.docker.com/compose/install/#install-compose

Build docker containers
```sh
docker-compose build
```

Run project (http://127.0.0.1:8000)
```sh
docker-compose up -d
```

Create a superuser using the command
```sh
./run_dev.sh python3 manage.py createsuperuser
```

Stop project
```sh
docker-compose down
```

Build docker containers without cache
```sh
docker-compose build --no-cache
```
