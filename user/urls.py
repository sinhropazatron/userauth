from django.urls import path

from .views import (user_detail, user_edit, user_list, user_login, user_logout,
                    user_profile, user_registration, verification_email,
                    send_verification_email, password_reset, password_change,
                    password_reset_confirm)

urlpatterns = [
    path('', user_list, name='user_list'),
    path('registration/', user_registration, name='registration'),
    path('verification_email/<slug:email_verification_code>/',
         verification_email, name='verification_email'),
    path('send_verification_email/', send_verification_email,
         name='send_verification_email'),
    path('login/', user_login, name='login'),
    path('logout/', user_logout, name='logout'),
    path('password_reset/', password_reset, name='password_reset'),
    path('password/reset/<uidb64>/<token>/', password_reset_confirm,
         name="password_reset_confirm"),
    path('password_change/', password_change, name='password_change'),
    path('profile/', user_profile, name='user_profile'),
    path('profile/edit/', user_edit, name='user_edit'),
    path('<slug:pk>/', user_detail, name='user_detail'),
    ]
