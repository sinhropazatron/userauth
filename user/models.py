import uuid

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import UserManager
from django.core.mail import EmailMessage
from django.db import models
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils import timezone

from .utils import generate_verification_code


class CustomUserManager(UserManager):
    def _create_user(self, email, password, is_staff=False, is_superuser=False,
                     commit=True, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        now = timezone.now()
        email = self.normalize_email(email).lower()
        user = self.model(email=email,
                          is_staff=is_staff, is_active=True,
                          is_superuser=is_superuser, last_login=now,
                          date_joined=now, **extra_fields)
        user.set_password(password)

        if commit:
            user.save(using=self._db)
        return user

    def create_user(self, email, password=None, commit=True,
                    **extra_fields):
        return self._create_user(email, password, commit=commit, **extra_fields)

    def create_superuser(self, email, password=None,  **extra_fields):
        return self._create_user(email, password, True, True, **extra_fields)


def get_profile_upload_to(instance, filename):
    new_filename = 'thumbnail.{}'.format(filename.split('.')[-1])
    return "users/{}/{}".format(instance.user.id, new_filename)


class User(AbstractUser):
    REGISTRATION_EMAIL_SUBJECT = 'Registration email from AuthUser.'
    CHANGED_EMAIL_SUBJECT = 'Email was changed.'
    REPEAT_EMAIL_VERIFICATION_EMAIL_SUBJECT = 'Email verification link.'

    username = None
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    email = models.EmailField(unique=True)
    screen_name = models.CharField(max_length=32)
    profile_picture = models.ImageField(upload_to=get_profile_upload_to,
                                        null=True, blank=True)
    email_verification_code = models.CharField(max_length=64, null=True,
                                               blank=True)
    email_expire_verification_code = models.DateTimeField(null=True, blank=True)
    email_verified = models.BooleanField(default=False)
    subscribed = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    password_entry_incorrect_count = models.PositiveSmallIntegerField(default=0)
    password_entry_incorrect_datetime = models.DateTimeField(null=True,
                                                             blank=True)

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['screen_name']

    class Meta:
        ordering = ('id', )
        verbose_name = 'user'
        verbose_name_plural = 'users'

    def get_absolute_url(self):
        return reverse('user_detail', kwargs={'pk': self.pk})

    def send_verification_email(self, mail_subject, template_name):
        if self.email_verified:
            return False

        delta = timezone.timedelta(seconds=settings.EMAIL_EXPIRE_TIME)
        expire_date = timezone.now() + delta
        self.email_expire_verification_code = expire_date
        self.email_verified = False
        self.email_verification_code = generate_verification_code(self)
        self.save()
        template_context = {
            'user': self,
            'site_address': settings.SITE_ADDRESS,
        }
        message = render_to_string(template_name, template_context)
        email = EmailMessage(
            mail_subject, message, to=[self.email]
        )
        email.content_subtype = "html"
        email.send()
        return True
