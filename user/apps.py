from django.apps import AppConfig


class UserConfig(AppConfig):
    name = 'user'

    def ready(self):
        super().ready()
        import user.signal_handlers
