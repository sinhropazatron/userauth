from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

from .models import User


@receiver(post_save, sender=User)
def send_registration_email(sender, instance, created, *args, **kwargs):
    if created:
        mail_subject = User.REGISTRATION_EMAIL_SUBJECT
        instance.send_verification_email(
            mail_subject,
            'user/email/registration_email.html'
        )

    elif not instance.email_verification_code:
        mail_subject = User.CHANGED_EMAIL_SUBJECT
        instance.send_verification_email(
            mail_subject,
            'user/email/changed_email.html'
        )


@receiver(pre_save, sender=User)
def send_verification_changed_email(sender, instance, *args, **kwargs):
    if instance.id:
        try:
            user = sender.objects.get(id=instance.id)
        except User.DoesNotExist:
            return

        if user.email != instance.email:
            instance.email_verification_code = None
            instance.email_verified = False
