from django import forms
from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm, \
    UserModel
from django.utils import timezone

from .models import User


class CustomUserCreationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('email', 'screen_name')

    def clean_email(self):
        email = self.cleaned_data['email']
        email = email.lower()
        return email

    def save(self, commit=True):
        email = self.cleaned_data.pop('email')
        password = self.cleaned_data.pop('password1')
        del self.cleaned_data['password2']

        user = User.objects.create_user(email, password, commit,
                                        **self.cleaned_data)
        return user

    def save_m2m(self):
        pass


class CustomAuthenticationForm(AuthenticationForm):
    def clean_username(self):
        username = self.cleaned_data['username']
        username = username.lower()
        return username

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username is not None and password:

            user_qs = UserModel.objects.filter(
                **{self.username_field.name: username}
            )

            user = None
            if user_qs.exists():
                user = user_qs.first()
                max_count = settings.INCORRECT_INPUT_PASSWORD_MAX_COUNT
                duration_lock = settings.INCORRECT_INPUT_PASSWORD_DURATION_LOCK
                t_delta = timezone.timedelta(seconds=duration_lock)
                started_lock_time = timezone.now() - t_delta
                user_incorrect_count = user.password_entry_incorrect_count
                user_incorrect_datetime = user.password_entry_incorrect_datetime
                # User is locked
                if user_incorrect_count >= max_count and \
                        user_incorrect_datetime > started_lock_time:
                    user.password_entry_incorrect_count += 1
                    user.save()
                    raise self.get_lock_login_error()

            self.user_cache = authenticate(self.request, username=username,
                                           password=password)
            if user and self.user_cache is None:
                # 1 - max_count incorrect input
                if user_incorrect_count < max_count:
                    user.password_entry_incorrect_count += 1
                    user.password_entry_incorrect_datetime = timezone.now()
                # incorrect input after locked
                elif user_incorrect_count >= max_count and \
                        user_incorrect_datetime < started_lock_time:
                    user.password_entry_incorrect_count = 1
                    user.password_entry_incorrect_datetime = timezone.now()
                else:
                    user.password_entry_incorrect_count = 1
                    user.password_entry_incorrect_datetime = timezone.now()
                user.save()

            if self.user_cache is None:
                raise self.get_invalid_login_error()
            else:
                if user:
                    user.password_entry_incorrect_count = 0
                    user.password_entry_incorrect_datetime = None
                    user.save()
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data

    def get_lock_login_error(self):
        return forms.ValidationError(
            'Account locked on {} minutes'.format(
                settings.INCORRECT_INPUT_PASSWORD_DURATION_LOCK / 60
            ),
            code='lock_login',
            params={'username': self.username_field.verbose_name},
        )


class UserEditForm(forms.ModelForm):
    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'email',
            'profile_picture',
            'screen_name',
            'subscribed',
        )

    def clean_email(self):
        email = self.cleaned_data['email']
        email = email.lower()
        return email
