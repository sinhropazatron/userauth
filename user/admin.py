from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _

from .models import User
from .forms import CustomUserCreationForm


class CustomUserAdmin(UserAdmin):
    list_display = ('id', 'last_name', 'first_name', 'screen_name', 'email',
                    'is_staff')

    fieldsets = (
        (_('Password info'), {
            'fields': ('password', 'password_entry_incorrect_count',
                       'password_entry_incorrect_datetime')
        }),
        (_('Personal info'), {
            'fields': ('first_name', 'last_name', 'profile_picture',
                       'screen_name', 'id')
        }),
        (_('Email info'), {
            'fields': ('email', 'email_verified', 'email_verification_code',
                       'email_expire_verification_code', 'subscribed')
        }),
        (_('Permissions'), {
            'classes': ('collapse',),
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups',
                       'user_permissions'),
        }),
        (_('Dates'), {
            'classes': ('collapse',),
            'fields': ('created', 'last_login', 'date_joined')
        }),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'screen_name', 'password1', 'password2'),
        }),
    )
    readonly_fields = ('created', 'id')
    search_fields = ('screen_name', 'first_name', 'last_name', 'email')
    ordering = ('id', )
    add_form = CustomUserCreationForm


admin.site.register(User, CustomUserAdmin)
