from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import (
    LoginView,
    LogoutView,
    PasswordResetView,
    PasswordChangeView,
    PasswordResetConfirmView
)
from django.shortcuts import redirect, get_object_or_404
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.generic import UpdateView, DetailView, ListView, CreateView, \
    View

from .forms import UserEditForm, CustomAuthenticationForm, \
    CustomUserCreationForm
from .models import User

UserModel = get_user_model()


class RegistrationView(CreateView):
    model = User
    form_class = CustomUserCreationForm
    template_name = "user/registration.html"

    def form_valid(self, form):
        response = super().form_valid(form)
        login(self.request, self.object)
        return response


class VerificationEmailView(View):
    def get(self, request, *args, **kwargs):
        v10n_code = self.kwargs.get('email_verification_code')
        user = get_object_or_404(User, email_verification_code=v10n_code,
                                 email_verified=False)
        if user.email_expire_verification_code < timezone.now():
            msg = 'Verification code is expired'
            messages.add_message(request, messages.WARNING, msg)
        else:
            user.email_verified = True
            msg = 'Email is verified.'
            messages.add_message(request, messages.SUCCESS, msg)
            user.save()
        return redirect(reverse('user_profile'))


@method_decorator(login_required, name='dispatch')
class SendVerificationEmailView(View):
    def get(self, request, *args, **kwargs):
        user = self.request.user
        if user.email_verified:
            msg = 'Email already verified'
            messages.add_message(request, messages.WARNING, msg)
            return redirect(reverse('user_edit'))

        t_delta = timezone.timedelta(seconds=settings.EMAIL_EXPIRE_TIME)
        last_send_date = user.email_expire_verification_code - t_delta
        t_delta = timezone.timedelta(
            seconds=settings.SEND_REPEAT_VERIFICATION_EMAIL_AFTER
        )
        next_can_send_date = last_send_date + t_delta

        if next_can_send_date < timezone.now():
            mail_subject = user.REPEAT_EMAIL_VERIFICATION_EMAIL_SUBJECT
            request.user.send_verification_email(
                mail_subject,
                'user/email/verification_email.html'
            )
            msg = 'Email was send.'
            messages.add_message(request, messages.SUCCESS, msg)
        else:
            msg = "You can't send a second email yet"
            messages.add_message(request, messages.WARNING, msg)
        return redirect(reverse('user_edit'))


@method_decorator(login_required, name='dispatch')
class UserProfile(DetailView):
    model = User
    template_name = 'user/user_profile.html'

    def get_object(self, queryset=None):
        return self.request.user


class CustomLoginView(LoginView):
    form_class = CustomAuthenticationForm
    template_name = 'user/login.html'

    def form_invalid(self, form):
        print(form.errors)
        return super().form_invalid(form)


@method_decorator(login_required, name='dispatch')
class UserEditView(UpdateView):
    model = User
    form_class = UserEditForm
    template_name = 'user/user_edit.html'
    success_url = reverse_lazy('user_edit')

    def get_object(self, queryset=None):
        return self.request.user

    def form_valid(self, form):
        messages.add_message(self.request, messages.SUCCESS, 'Profile updated')
        return super().form_valid(form)


class UserListView(ListView):
    model = User
    queryset = User.objects.all()
    template_name = 'user/user_list.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        ctx = super().get_context_data()
        ctx['head_title'] = 'A list of users'
        return ctx


class UserDetailView(DetailView):
    model = User


class CustomPasswordChangeView(PasswordChangeView):
    template_name = 'user/password_change.html'
    success_url = reverse_lazy('user_edit')

    def form_valid(self, form):
        msg = 'Your password has been set.'
        messages.add_message(self.request, messages.SUCCESS, msg)
        return super().form_valid(form)


class CustomPasswordResetView(PasswordResetView):
    template_name = 'user/password_reset_form.html'
    success_url = reverse_lazy('user_edit')

    def form_valid(self, form):
        msg = 'You have been sent an email with a link to the password reset'
        messages.add_message(self.request, messages.SUCCESS, msg)
        return super().form_valid(form)


class CustomPasswordResetConfirmView(PasswordResetConfirmView):
    template_name = 'user/password_reset_confirm.html'
    success_url = reverse_lazy('login')

    def form_valid(self, form):
        msg = 'Your password has been set.  You may go ahead and log in now.'
        messages.add_message(self.request, messages.SUCCESS, msg)
        return super().form_valid(form)


user_registration = RegistrationView.as_view()
verification_email = VerificationEmailView.as_view()
send_verification_email = SendVerificationEmailView.as_view()
user_profile = UserProfile.as_view()
user_logout = LogoutView.as_view()
user_login = CustomLoginView.as_view()
user_edit = UserEditView.as_view()
user_list = UserListView.as_view()
user_detail = UserDetailView.as_view()
password_change = CustomPasswordChangeView.as_view()
password_reset = CustomPasswordResetView.as_view()
password_reset_confirm = CustomPasswordResetConfirmView.as_view()
