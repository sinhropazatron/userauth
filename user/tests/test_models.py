from django.test import TestCase
from user.models import User
from user.tests import TEST_USER_EMAIL, TEST_REGISTRATION_EMAIL
from django.core import mail


class UserTestClass(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(email=TEST_REGISTRATION_EMAIL)

    def test_case_insensitive_email(self):
        self.assertEquals(self.user.email, TEST_USER_EMAIL)

    def test_get_absolute_url(self):
        self.assertEquals(self.user.get_absolute_url(),
                          '/user/{}/'.format(self.user.id))

    def test_registration_email_sending(self):
        self.assertEquals(len(mail.outbox), 1)
        self.assertEquals(mail.outbox[0].subject,
                          User.REGISTRATION_EMAIL_SUBJECT)
        self.assertEquals(mail.outbox[0].to, [self.user.email])

    def test_changed_email_sending(self):
        changed_email = 'test_changed@example.com'
        self.user.email = changed_email
        self.assertEquals(len(mail.outbox), 1)
        self.user.save()
        self.assertEquals(len(mail.outbox), 2)
        self.assertEquals(mail.outbox[1].subject, User.CHANGED_EMAIL_SUBJECT)
        self.assertEquals(mail.outbox[1].to, [changed_email])
        self.assertEquals(changed_email, self.user.email)
