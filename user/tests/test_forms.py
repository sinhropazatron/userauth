from django.test import TestCase
from user.forms import CustomUserCreationForm
from user.tests import TEST_USER_EMAIL, TEST_REGISTRATION_EMAIL, \
    TEST_SCREEN_NAME


class CustomUserCreationFormTest(TestCase):
    def test_creation_user_form_fields(self):
        form = CustomUserCreationForm()
        self.assertTrue('email' in form.fields and 'screen_name' in form.fields)

    def test_creation_user_form_required_fields(self):
        form = CustomUserCreationForm()
        self.assertTrue(
            form.fields['email'].required is True and
            form.fields['screen_name'].required is True
        )

    def test_creation_user_form_case_insensitive_email(self):
        form_data = {
            'email': TEST_REGISTRATION_EMAIL,
            'screen_name': TEST_SCREEN_NAME
        }
        form = CustomUserCreationForm(form_data)
        form.is_valid()
        self.assertEqual(form.clean_email(), TEST_USER_EMAIL)
