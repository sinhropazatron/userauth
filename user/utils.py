import hashlib
from django.utils.crypto import get_random_string


def generate_verification_code(value):
    hash_input = bytes('{}_{}'.format(get_random_string(5), value), 'utf-8')
    activation_key = hashlib.sha256(hash_input).hexdigest()
    return activation_key
