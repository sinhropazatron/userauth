#!/bin/bash

if [[ ${LOCAL_USER_ID} -eq "" ]]; then
    echo "LOCAL_USER_ID is not set"
    exit 1
fi
if [[ $# -lt 1 ]]; then
    echo "No command provided"
    exit 0
fi
useradd --shell /bin/bash -u ${LOCAL_USER_ID} -o -m developer && gosu developer $@
