#!/bin/bash

set -e

USERNAME="user_name``"
DBNAME="project_db"

createuser --username "${POSTGRES_USER}" -d ${USERNAME} && \
createdb --username "${POSTGRES_USER}" -O ${USERNAME} ${DBNAME}
